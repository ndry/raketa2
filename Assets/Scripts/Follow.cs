﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Follow : MyMonoBehaviour
{
    public Transform target;
    public Vector3 offset;
    public float velocityFactor = 1;

    public LazyChild<LineRenderer> SecondTargetLine;
    public LazyChild<LineRenderer> RelativeVelocityLine;
    public LazyChild<LineRenderer> ReachVelocityLine;
    public LazyChild<LineRenderer> ReachVelocitySumLine;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (!target) {
            return;
        }
        var targetHandler = target.GetComponent<TargetHandler>();
        var secondTarget = targetHandler.target;
        var autoPilot = target.GetComponent<AutoPilot>();
        transform.position = target.position
            + offset
            + offset.normalized
                * target.GetComponent<Rigidbody>().velocity.magnitude
                * target.GetComponent<Rigidbody>().velocity.magnitude
                * velocityFactor;
        transform.rotation = target.rotation;

        if (SecondTargetLine.Value) {
            if (secondTarget) {
                SecondTargetLine.Value.enabled = true;
                SecondTargetLine.Value.SetPosition(0, 
                    target.position
                        + Vector3.forward * 5);
                SecondTargetLine.Value.SetPosition(1, 
                    target.position
                        + target.transform.TransformVector(targetHandler.Position)
                        + Vector3.forward * 5);
            } else {
                SecondTargetLine.Value.enabled = false;
            }
        }

        if (RelativeVelocityLine.Value) {
            if (secondTarget) {
                RelativeVelocityLine.Value.enabled = true;
                RelativeVelocityLine.Value.SetPosition(0, 
                    target.position
                        + Vector3.forward * 4);
                RelativeVelocityLine.Value.SetPosition(1, 
                    target.position
                        + target.transform.TransformVector(-targetHandler.Velocity)
                        + Vector3.forward * 4);
            } else {
                RelativeVelocityLine.Value.enabled = false;
            }
        }

        if (ReachVelocityLine.Value) {
            if (secondTarget) {
                ReachVelocityLine.Value.enabled = true;
                ReachVelocityLine.Value.SetPosition(0, 
                    target.position
                        + Vector3.forward * 3);
                ReachVelocityLine.Value.SetPosition(1, 
                    target.position
                        + target.transform.TransformVector(autoPilot.ReachVelocityOutput)
                        + Vector3.forward * 3);
            } else {
                ReachVelocityLine.Value.enabled = false;
            }
        }

        if (ReachVelocitySumLine.Value) {
            if (secondTarget) {
                ReachVelocitySumLine.Value.enabled = true;
                ReachVelocitySumLine.Value.SetPosition(0, 
                    target.position
                        + Vector3.forward * 3);
                ReachVelocitySumLine.Value.SetPosition(1, 
                    target.position
                        + target.transform.TransformVector(autoPilot.ReachVelocityOutput - targetHandler.Velocity)
                        + Vector3.forward * 3);
            } else {
                ReachVelocitySumLine.Value.enabled = false;
            }
        }
    }
}
