﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MyMonoBehaviour
{

    new Rigidbody rigidbody { get => GetComponent<Rigidbody>(); }
    Spaceship spaceship { get => GetComponent<Spaceship>(); }
    TargetHandler targetHandler { get => GetComponent<TargetHandler>(); }
    AutoPilot autoPilot { get => GetComponent<AutoPilot>(); }

    Engine.Intensivity GetModifiedIntensivity(
        bool on,
        Engine.Intensivity baseIntensivity
    ) {
        if (!on) {
            return Engine.Intensivity.Off;
        }
        if (Input.GetKey(KeyCode.LeftControl)) {
            baseIntensivity = Engine.IntensivityUp(baseIntensivity);
        }
        if (Input.GetKey(KeyCode.LeftShift)) {
            baseIntensivity = Engine.IntensivityDown(baseIntensivity);
        }
        return baseIntensivity;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (
            Input.GetKey(KeyCode.D)
            || Input.GetKey(KeyCode.A)
            || Input.GetKey(KeyCode.W)
            || Input.GetKey(KeyCode.S)
            || Input.GetKey(KeyCode.Q)
            || Input.GetKey(KeyCode.E)
            || Input.GetKey(KeyCode.R)
            || Input.GetKey(KeyCode.T)
            || Input.GetKey(KeyCode.F)
            || Input.GetKey(KeyCode.G)
            || Input.GetKey(KeyCode.Space)
        ) {
            autoPilot.enabled = false;
        }

        if (autoPilot.enabled) {
            return;
        }
        
        spaceship.EngineTopLeft.Value.intensivity = GetModifiedIntensivity(
                Input.GetKey(KeyCode.D)
                || Input.GetKey(KeyCode.E)
                || Input.GetKey(KeyCode.R),
                Engine.Intensivity.Cruise); 

        spaceship.EngineTopRight.Value.intensivity = GetModifiedIntensivity(
                Input.GetKey(KeyCode.A)
                || Input.GetKey(KeyCode.Q)
                || Input.GetKey(KeyCode.T),
                Engine.Intensivity.Cruise); 

        spaceship.EngineBottomLeft.Value.intensivity = GetModifiedIntensivity(
                Input.GetKey(KeyCode.A)
                || Input.GetKey(KeyCode.E)
                || Input.GetKey(KeyCode.F),
                Engine.Intensivity.Cruise); 

        spaceship.EngineBottomRight.Value.intensivity = GetModifiedIntensivity(
                Input.GetKey(KeyCode.D)
                || Input.GetKey(KeyCode.Q)
                || Input.GetKey(KeyCode.G),
                Engine.Intensivity.Cruise); 

        spaceship.EngineTop.Value.intensivity = GetModifiedIntensivity(
            Input.GetKey(KeyCode.S),
            Engine.Intensivity.Cruise);

        spaceship.EngineMain.Value.intensivity = Input.GetKey(KeyCode.W)
            ? GetModifiedIntensivity(
                Input.GetKey(KeyCode.W),
                Engine.Intensivity.Maneur)
            : GetModifiedIntensivity(
                Input.GetKey(KeyCode.Space),
                Engine.Intensivity.Cruise);
    }
}
