﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParentDirectionDisplay : MonoBehaviour
{    
    new Rigidbody rigidbody {
        get {
            return transform.parent.GetComponent<Rigidbody>();
        }
    }
    LineRenderer lineRenderer {
        get {
            return GetComponent<LineRenderer>();
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        var start = new Vector3(0, 0, 4);
        lineRenderer.startWidth = Math.Max(0.5f, rigidbody.velocity.magnitude * 0.1f);
        lineRenderer.SetPosition(0, new Vector3(0, 0, 4));
        lineRenderer.SetPosition(1, lineRenderer.GetPosition(0) + 
            Vector3.up * Math.Max(rigidbody.velocity.magnitude * 0.5f, 2.5f));
        lineRenderer.SetPosition(2, lineRenderer.GetPosition(1) + 
            Vector3.left
            * rigidbody.angularVelocity.z * lineRenderer.startWidth * 3);
    }
}
