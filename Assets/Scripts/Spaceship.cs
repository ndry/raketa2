﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class Spaceship : MyMonoBehaviour
{
    public new Rigidbody rigidbody { get => GetComponent<Rigidbody>(); }

    public LazyChild<Engine> EngineMain;
    public LazyChild<Engine> EngineTop;
    public LazyChild<Engine> EngineTopLeft;
    public LazyChild<Engine> EngineTopRight;
    public LazyChild<Engine> EngineBottomLeft;
    public LazyChild<Engine> EngineBottomRight;

    public LazyChild<Transform> AtmosphericDrag;
    public LazyChild<Transform> AtmosphericDrag_Parashute;
    public LazyChild<Transform> AtmosphericDrag_RandomRotaion;
    public LazyChild<Transform> AtmosphericDrag_RandomRotaion2;
    public LazyChild<MeshRenderer> AtmosphericDrag_RandomRotaion_Sphere;
    public LazyChild<MeshRenderer> AtmosphericDrag_RandomRotaion2_Sphere;

    // Start is called before the first frame update
    void Start()
    {
        rigidbody.maxAngularVelocity = 20;
    }

    // Update is called once per frame
    void Update()
    {
        var advancedPhysics = GetComponent<AdvancedPhysics>();
        var dragForce = advancedPhysics.DragForceOutput;
        var a = Mathf.Min(1, dragForce.magnitude / 40);
        AtmosphericDrag_RandomRotaion_Sphere.Value.material.color
            = new Color(1, 1, 1, a);
        AtmosphericDrag_RandomRotaion2_Sphere.Value.material.color
            = new Color(1, 1, 1, a / 2);

        AtmosphericDrag.Value.rotation
            = Quaternion.LookRotation(Vector3.forward, dragForce);
        AtmosphericDrag_RandomRotaion.Value.localRotation
            = Quaternion.Euler(0, Random.Range(-180f, 180f), 0);
        AtmosphericDrag_RandomRotaion2.Value.localRotation
            = Quaternion.Euler(0, Random.Range(-180f, 180f), 0);

        AtmosphericDrag_Parashute.Value.gameObject
            .SetActive(advancedPhysics.hasParashute);

        var aa = Math.Min(0.4f, Mathf.Max(0.3f - a / 2, 0.2f));
        AtmosphericDrag_Parashute.Value.localScale = new Vector3(
            aa, 
            (1 / (2 * aa) - aa) / 2, 
            aa);
        AtmosphericDrag_Parashute.Value.transform.localPosition
            = Vector3.up * (1 + 1 / aa);
    }
}
