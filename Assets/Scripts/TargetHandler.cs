﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetHandler : MyMonoBehaviour
{
    public Transform target;

    public Vector3 Position {
        get => transform.InverseTransformPoint(target.position);
    }

    public float Angle {
        get => Quaternion.LookRotation(Vector3.forward, Position)
            .eulerAngles.z;
    }

    public Vector3 Velocity {
        get => transform.InverseTransformVector(
            FindGrandparentRigidbody(target).GetPointVelocity(target.position)
                - GetComponent<Rigidbody>().velocity);
    }
}
