﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpaceColonyAnimation : MyMonoBehaviour
{
    public LazyChild<Transform> GameObject_Station_Rotor;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        GameObject_Station_Rotor.Value.localRotation =
            Quaternion.Euler(
                0,
                Time.timeSinceLevelLoad * 5f,
                0
            );
    }
}
