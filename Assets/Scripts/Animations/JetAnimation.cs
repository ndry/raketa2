﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JetAnimation : MyMonoBehaviour
{
    public float intensivity = 1;

    public LazyChild<Light> BigPointLight;
    public LazyChild<Light> SmallPointLight;
    public LazyChild<Transform> OptimizedTree;
    float bigPointLightOriginalRange;
    float smallPointLightOriginalRange;

    // Start is called before the first frame update
    void Start()
    {
        bigPointLightOriginalRange = BigPointLight.Value.range;
        smallPointLightOriginalRange = SmallPointLight.Value.range;
    }

    // Update is called once per frame
    void Update()
    {
        transform.localEulerAngles = Vector3.up * Random.Range(0f, 360f);
        transform.localScale = Vector3.one + Vector3.up * Random.Range(-0.3f, 0.3f);
        OptimizedTree.Value.localScale = 
            Vector3.one
            + Vector3.up * (intensivity * intensivity - 1);
        BigPointLight.Value.range = bigPointLightOriginalRange
            * intensivity
            * BigPointLight.Value.transform.lossyScale.magnitude / Vector3.one.magnitude;
        SmallPointLight.Value.range = smallPointLightOriginalRange
            * intensivity
            * SmallPointLight.Value.transform.lossyScale.magnitude / Vector3.one.magnitude;
    }
}
