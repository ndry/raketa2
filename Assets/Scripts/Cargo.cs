﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cargo : MonoBehaviour
{
    static string[] CargoIds = {
        "Пахуча сосна",
        "Гігантовий кедр",
        "Текуча береза",
        "Плакуча верба",
        "Криваво-м'ясний гіркокаштан",
        "Кривочашечковий глід",
        "Віргінський ялівець",
        "Рання яблуня",
        "Необичний дуб",
        
        "Амурський бархат",
        "Небесний лайм",
        "Устраюяща липа",
        "Клен-вертольот",
        "Реліктовий гінкго",
    };

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
