﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RigidbodySetup : MonoBehaviour
{
    public Vector3 velocity;
    public Vector3 angularVelocity;
    public bool onStart;
    public bool onFixedUpdate;

    new Rigidbody rigidbody { get => GetComponent<Rigidbody>(); }

    
    // Start is called before the first frame update
    void Start()
    {
        if (onStart) {
            rigidbody.velocity = velocity;
            rigidbody.angularVelocity = angularVelocity;
        }
    }

    void FixedUpdate() {
        if (onFixedUpdate) {
            rigidbody.velocity = velocity;
            rigidbody.angularVelocity = angularVelocity;
        }
    }

    // Update is called once per frame
    void Update()
    {
    }
}
