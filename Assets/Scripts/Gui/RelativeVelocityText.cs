﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RelativeVelocityText : UserInterfaceBehaviour
{
    Rigidbody FindGrandparentRigidbody(Transform t) {
        for (; t; t = t.parent) {
            var r = t.GetComponent<Rigidbody>();
            if (r) {
                return r;
            }
        }
        return null;
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        var distance = userInterface.secondTarget
            ? (
                FindGrandparentRigidbody(userInterface.secondTarget.transform).GetPointVelocity(userInterface.secondTarget.transform.position)
                - userInterface.target.GetComponent<Rigidbody>().velocity
            ).magnitude.ToString()
            : "N / A";

        GetComponent<TMPro.TextMeshProUGUI>().text = $"Відносна швидкість: {distance}";
    }
}
