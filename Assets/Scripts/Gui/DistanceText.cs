﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DistanceText : UserInterfaceBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        var distance = userInterface.secondTarget
            ? (userInterface.secondTarget.transform.position - userInterface.target.transform.position).magnitude.ToString()
            : "N / A";

        GetComponent<TMPro.TextMeshProUGUI>().text = $"Відстань: {distance}";
    }
}
