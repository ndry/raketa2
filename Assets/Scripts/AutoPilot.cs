﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using UnityEngine;

public class AutoPilot : MyMonoBehaviour
{

    new Rigidbody rigidbody { get => GetComponent<Rigidbody>(); }
    Spaceship spaceship { get => GetComponent<Spaceship>(); }
    TargetHandler targetHandler { get => GetComponent<TargetHandler>(); }
    
    public bool stopRotationRequested;
    public bool stopRotationAtAngleRequested;
    public float stopRotationAtAngle;
    public bool reachTargetRequested;
    public float reachTargetSafeRadius;
    public bool reachOrbitalSpeedRequested;


    public Vector3 ReachVelocityOutput { get; private set; }
    

    public void Turn(float direction, Engine.Intensivity intensivity) {
        if (direction < 0) {
            spaceship.EngineTopLeft.Value.intensivity = intensivity;
            spaceship.EngineBottomRight.Value.intensivity = intensivity;
        }
        if (direction > 0) {
            spaceship.EngineTopRight.Value.intensivity = intensivity;
            spaceship.EngineBottomLeft.Value.intensivity = intensivity;
        }
    }
    
    float lastAvz;
    // Update is called once per frame
    void FixedUpdate()
    {
        if (stopRotationRequested) {
            if (ReachAngularVelocity(0)) {
                stopRotationRequested = false;
            }
        }

        if (stopRotationAtAngleRequested) {
            if (ReachAngle(targetHandler.Angle + stopRotationAtAngle)) {
                stopRotationAtAngleRequested = false;
            }
        }

        if (reachTargetRequested) {
            if (ReachTarget(
                targetHandler.Position, 
                targetHandler.Velocity, 
                reachTargetSafeRadius
            )) {
                reachTargetRequested = false;
            }
        }

        reachOrbitalSpeedRequested = reachOrbitalSpeedRequested
            && !ReachOrbitalSpeed();

        lastAvz = rigidbody.angularVelocity.z;
    }

    [Pure]
    public static float NormalizeAngle(float a) {
        while (a > 180) { a -= 360; }
        while (a < -180) { a += 360; }
        return a;
    }

    [Pure]
    public float CalcTargetAngularVelocity(float angle) {
        angle = NormalizeAngle(angle);
        return Mathf.Sign(angle) * Mathf.Sqrt(Mathf.Abs(angle)) * 0.1f;
    }

    public bool ReachAngularVelocity(float targetAngularVelocity) {
        var angularVelocityDiff = rigidbody.angularVelocity.z - targetAngularVelocity;
        if (Math.Abs(angularVelocityDiff) > 0.3) {
            Turn(-angularVelocityDiff, Engine.Intensivity.Cruise);
        } else if (Math.Abs(angularVelocityDiff) > 0.03) {
            Turn(-angularVelocityDiff, Engine.Intensivity.Maneur);
        } else if (Math.Abs(angularVelocityDiff) > 0.003) {
            if (rigidbody.angularVelocity.z == lastAvz) {
                Turn(-angularVelocityDiff, Engine.Intensivity.Maneur);
            }
        } else {
            return true;
        }
        return false;
    }

    public bool ReachAngle(float targetAngle) {
        targetAngle = NormalizeAngle(targetAngle);
        var targetAngularVelocity =
            (Mathf.Abs(rigidbody.angularVelocity.z) > 4)
                ? 3
                : CalcTargetAngularVelocity(targetAngle);
        
        if (ReachAngularVelocity(targetAngularVelocity)) {
            if (Mathf.Abs(NormalizeAngle(targetAngle)) < 0.05) {
                return true;
            }
        }

        return false;
    }

    public bool ReachVelocity(Vector3 velocity) {
        ReachVelocityOutput = velocity;
        
        var m = rigidbody.mass;
        var cf = spaceship.EngineMain.Value
            .GetForceMagnitude(Engine.Intensivity.Cruise);
        var mf = spaceship.EngineMain.Value
            .GetForceMagnitude(Engine.Intensivity.Maneur);
        var dt = Time.fixedDeltaTime;
        var cdv = cf * m * dt * 2;
        var mdv = mf * m * dt * 2;

        if (velocity.magnitude <= mdv) {
            return true;
        }

        var ta = Quaternion.LookRotation(Vector3.forward, velocity).eulerAngles.z;
        
        ReachAngle(ta);
        if (Mathf.Abs(NormalizeAngle(ta)) < 1) {
            if (velocity.magnitude > cdv) {
                spaceship.EngineMain.Value.intensivity = Engine.Intensivity.Cruise;
            } else if (velocity.magnitude > mdv) {
                spaceship.EngineMain.Value.intensivity = Engine.Intensivity.Maneur;
            } else {
                return true;
            }
        }

        return false;
    }

    public bool ReachTarget(
        Vector3 targetPosition,
        Vector3 targetVelocity,
        float safeRadius
    ) {
        var m = rigidbody.mass;
        var f = spaceship.EngineMain.Value.GetForceMagnitude(Engine.Intensivity.Cruise);
        
        var brakingRotationTime = 4f;
        var dn = targetPosition.normalized;
        var dm = Mathf.Max(0, 
                targetPosition.magnitude
                    - targetVelocity.magnitude * brakingRotationTime
                    - safeRadius);

        var ttv = Mathf.Sqrt(2 * m * f * dm) * dn;

        if ((targetPosition.magnitude - safeRadius) < 20) {
            return ReachVelocity(targetVelocity) && ReachAngularVelocity(0);
        }

        ReachVelocity(ttv + targetVelocity);

        return false;
    }

    public bool ReachOrbitalSpeed() {
        var targetRigidbody = targetHandler.target.GetComponent<Rigidbody>();
        if (!targetRigidbody || targetRigidbody.mass < (rigidbody.mass * 1000)) {
            return true;
        }

        var vm = Mathf.Sqrt(
            AdvancedPhysics.GravitationalConstant
            * targetRigidbody.mass
            / targetHandler.Position.magnitude);

        var va = targetHandler.Angle + 90;
        var vn = new Vector3(
            -Mathf.Sin(va * Mathf.Deg2Rad), 
            Mathf.Cos(va * Mathf.Deg2Rad), 
            0);

        var v = vn * vm;

        return ReachVelocity(v + targetHandler.Velocity);
    }
}
