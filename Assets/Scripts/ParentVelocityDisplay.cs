﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParentVelocityDisplay : MonoBehaviour
{
    new Rigidbody rigidbody {
        get {
            return transform.parent.GetComponent<Rigidbody>();
        }
    }
    LineRenderer velocityDisplay {
        get {
            return GetComponent<LineRenderer>();
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        velocityDisplay.startWidth = 5;
        var velocityDisplayStart = transform.position + new Vector3(0, 0, 5);
        velocityDisplay.SetPosition(0, velocityDisplayStart);
        velocityDisplay.SetPosition(1, velocityDisplayStart + rigidbody.velocity);
    }
}
