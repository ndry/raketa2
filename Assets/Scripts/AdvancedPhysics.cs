﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AdvancedPhysics : MonoBehaviour
{
    public static float GravitationalConstant = 100f; 

    new Rigidbody rigidbody { get => transform.GetComponent<Rigidbody>(); }

    public Vector3 GetGvavitationalAcceleration(Vector3 distance) 
        => GravitationalConstant
            * rigidbody.mass
            / distance.sqrMagnitude
            * distance.normalized;
    
    public float atmosphericDensityAtSurface = 0f;
    public float atmosphericDensityHalfDropAltitude = 0f;
    public float surface = 0f;
    public bool hasParashute = false;

    public float GetAtmosphericDensity(float distance)
        => atmosphericDensityAtSurface
            * Mathf.Pow(
                0.5f,
                (distance - surface) / atmosphericDensityHalfDropAltitude);

    
    public Vector3 DragForceOutput { get; private set; }
    void FixedUpdate()
    {
        var gravityObjects = Resources.FindObjectsOfTypeAll(typeof(AdvancedPhysics));
        foreach (AdvancedPhysics o in gravityObjects) {
            if (o == this) {
                continue;
            }
            if (!o.isActiveAndEnabled) {
                continue;
            }

            var d = (o.transform.position - this.transform.position);

            this.rigidbody.AddForce(rigidbody.mass * o.GetGvavitationalAcceleration(d));

            if (o.atmosphericDensityAtSurface > 0) {
                
                var ro = o.GetAtmosphericDensity(d.magnitude);
                var v = rigidbody.velocity - o.rigidbody.velocity;
                var dragForce = ro * v.magnitude * -v * (hasParashute ? 4 : 1);

                Debug.Log(
                    $"atmosphericDensity {o.GetAtmosphericDensity(d.magnitude)}"
                    + $" dragForce {dragForce} {dragForce.magnitude}");

                this.rigidbody.AddForce(dragForce);

                DragForceOutput = dragForce;
            } else {
                DragForceOutput = Vector3.zero;
            }
        }
    }
}
