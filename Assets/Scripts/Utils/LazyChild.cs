using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using UnityEngine;

public class LazyChild {
    public static IEnumerable<FieldInfo> GetFieldsOfInterest(MonoBehaviour owner) {
        return owner.GetType()
            .GetFields()
            .Where(f =>
                f.FieldType.IsGenericType
                && f.FieldType.GetGenericTypeDefinition() == typeof(LazyChild<>));
    }
    public static void AssignAll(MonoBehaviour owner) {
        foreach (var f in GetFieldsOfInterest(owner)) {
            typeof(LazyChild)
                .GetMethod("AssignNew", new Type[] { typeof(MonoBehaviour), typeof(string) })
                .MakeGenericMethod(f.FieldType.GetGenericArguments()[0])
                .Invoke(null, new object[] { owner, f.Name });
        }
    }

    public static void InvalidateAll(MonoBehaviour owner) {
        foreach (var f in GetFieldsOfInterest(owner)) {
            f.FieldType
                .GetMethod("InvalidateCache")
                .Invoke(f.GetValue(owner), null);
        }
    }

    public static LazyChild<T> AssignNew<T>(
        MonoBehaviour owner, Expression<Func<LazyChild<T>>> fieldExpr
    ) where T : Component {
        var fieldName = (fieldExpr.Body as MemberExpression).Member.Name;
        return AssignNew<T>(owner, fieldName);
    }

    public static LazyChild<T> AssignNew<T>(
        MonoBehaviour owner, string fieldName
    ) where T : Component {
        var path = fieldName.Replace("_", "/");
        var lc = new LazyChild<T>(owner, path);
        owner.GetType().GetField(fieldName).SetValue(owner, lc);
        return lc;
    }
}

public class LazyChild<T> where T : Component {
    private MonoBehaviour owner;
    private string path;
    private WeakReference<T> cachedValueReference;

    public LazyChild(MonoBehaviour owner, string path) {
        this.owner = owner;
        this.path = path;
    }

    public void InvalidateCache() {
        cachedValueReference = null;
    }

    public T CalcValue() {
        return (T)owner.transform.Find(path)?.GetComponent(typeof(T));
    }

    public T Value {
        get {
            if (cachedValueReference == null) {
                cachedValueReference = new WeakReference<T>(CalcValue());
            }
            cachedValueReference.TryGetTarget(out var c);
            return c;
        }
    }

    public static implicit operator T(LazyChild<T> lc) {
        return lc.Value;
    }
}
