﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextureNoiseGPU : MonoBehaviour {
    public bool keepPlaneDistance = false;
    public float planeDistanceFactor = 1;
    [Header("Seed options")]
    public bool useSeed = false;
    public int seed = 0;
    
    RawImage image {
        get {
            return GetComponentInChildren<RawImage>();
        }
    }

    RenderTexture texture {
        get {
            var t = image.texture as RenderTexture;

            if (t && (t.width != Screen.width || t.height != Screen.height)) {
                Destroy(t);
                image.texture = t = null;
            }

            if (!t) {
                image.texture = t = new RenderTexture(Screen.width, Screen.height, 0) {
                    enableRandomWrite = true
                };
                t.Create();
                t.filterMode = FilterMode.Point;
            }
            return t;
        }
    }

    // Use this for initialization
    void Start() {
        MakeTexture();
    }

    void Update() {
        MakeTexture();
    }

    void MakeTexture() {
        if (useSeed) {
            NoiseS3D.seed = seed;
        }

        var canvas = GetComponents<Canvas>()[0];

        if (keepPlaneDistance) {
            canvas.planeDistance = -canvas.worldCamera.transform.position.z * planeDistanceFactor;
        }

        RenderNoise(texture, transform.localToWorldMatrix, Time.realtimeSinceStartup, canvas.planeDistance);
    }

    public static void RenderNoise(RenderTexture texture, Matrix4x4 transform, float time, float distance) {
		ComputeShader shader = Resources.Load("Shaders/NoiseS3DGPU") as ComputeShader;
		
		int[] resInts = { texture.width, texture.height };
		
		int kernel = shader.FindKernel("ComputeNoise");
		shader.SetTexture(kernel, "Result", texture);
		shader.SetFloat("distance", distance);
		shader.SetMatrix("transform", transform);
		shader.SetFloat("time", time);
		shader.SetInts("reses", resInts);
		
		ComputeBuffer permBuffer = new ComputeBuffer(NoiseS3D.perm.Length, 4);
		permBuffer.SetData(NoiseS3D.perm);
		shader.SetBuffer(kernel, "perm", permBuffer);
		
		shader.Dispatch(kernel, Mathf.CeilToInt(texture.width / 16f), Mathf.CeilToInt(texture.height / 16f), 1);
		
		permBuffer.Release();
	}

}
